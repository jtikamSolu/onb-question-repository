==========
Objective: Write code to modify the DOM structure
==========

[[ASK: too low level or should be in jQuery section?]]

[[
You are building a web page that provides a glossary of terms.  The source HTML
merely includes the skeleton of a definition list:

<pre>&LT;dl id='glossaryList'&GT;&LT;/dl&GT;</pre>

Furthermore, a service provides a JSON map of terms to definitions like this:

<pre>var termMap = { 'Java' : 'An enterprise-scale programming language',
		  'JavaScript' : 'The standard programming language of the web',
		  'XML' : 'A data exchange language' };</pre>

Which JavaScript function creates the DOM element to populate the definition list?
]]
*1: <pre>function populateGlossary(termMap) {
	var dl = document.getElementById('glossaryList');
	for (var term in termMap) {
		var dt = document.createElement('dt');
		dt.innerHTML = term;
		dl.appendChild(dt);
		var dd = document.createElement('dd');
		dd.innerHTML = termMap[term];
		dl.appendChild(dd);
	}
}</pre>
2: <pre>function populateGlossary(termMap) {
	var dl = document.getElementById('glossaryList');
	for (var term in termMap) {
		var dt = document.createTag('dt');
		dt.innerHTML = term;
		dl.appendChild(dt);
		var dd = document.createTag('dd');
		dd.innerHTML = termMap[term];
		dl.appendChild(dd);
	}
}</pre>
3: <pre>function populateGlossary(termMap) {
	var dl = document.getElementById('glossaryList');
	for (var term in termMap) {
		var dt = document.createTag('dt');
		dt.innerHTML = term;
		dl.addChild(dt);
		var dd = document.createTag('dd');
		dd.innerHTML = termMap[term];
		dl.addChild(dd);
	}
}</pre>
4: <pre>function populateGlossary(termMap) {
	var dl = document.getElementById('glossaryList');
	for (var term in termMap) {
		var dt = document.createElement('dt');
		dt.innerHTML = term;
		dl.addChild(dt);
		var dd = document.createElement('dd');
		dd.innerHTML = termMap[term];
		dl.addChild(dd);
	}
}</pre>
