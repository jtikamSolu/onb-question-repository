(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Hayden Wagner
(Course Site): Code School
(Course Name): JavaScript Roadtrip: Part 3
(Course URL): http://javascript-roadtrip-part3.codeschool.com/levels/1/challenges/1
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): Select the true statements about function expressions (choose 2)
(A): They are built and stored in program memory as soon as the program loads 
(B): They load into memory only when the line of code with the assignment statement is reached
(C): Functions in function expressions always require a name
(D): You should put a semicolon after the last bracket of the function expression because it is an assignment statement
(E): You shouldn’t put a semicolon after a function expression because it is unnecessary
(Correct): B, D
(Points): 1
(CF): Function expressions are not stored in memory or accessible in a program until the line of code where they are assigned to a variable is reached. You don’t need to name the function in a function expression because the variable it is assigned to will serve as the name.
(WF): Function expressions are not stored in memory or accessible in a program until the line of code where they are assigned to a variable is reached. You don’t need to name the function in a function expression because the variable it is assigned to will serve as the name.
(STARTIGNORE)
(Hint):
(Subject): Function Expressions
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Function expressions give flexibility in choosing which functionality to build because they allow us to assign different function values to variables based on conditional logic
(A): True
(B): False
(Correct): A
(Points): 1
(CF): The same variable can have different function expressions assigned to it based on logical conditions, which gives a program flexibility that it doesn't have with only function declarations.
(WF): The same variable can have different function expressions assigned to it based on logical conditions, which gives a program flexibility that it doesn't have with only function declarations.
(STARTIGNORE)
(Hint):
(Subject): Function Expressions
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): Choose the correct statements regarding closures (choose 2) 
(A): A closure occurs whenever a program returns a function from another function
(B): A closure occurs when a program returns a function from within another function and the returned function is complete with variables that showed up in the external function. 
(C): Closure is the coding practice of closing all your function expressions with brackets and including semicolons after the closing brackets
(D): Closure functions can modify bound variables in the background
(E): A closure is when both global and local variables are used in the same scope
(Correct): B, D
(Points): 1
(CF): Closures occur when a function returns another function, but the returned function must include variables from the external function. The external function can modify these variables and the internal function will reference the correct value when it is called because of the concept of closure.
(WF): Closures occur when a function returns another function, but the returned function must include variables from the external function. The external function can modify these variables and the internal function will reference the correct value when it is called because of the concept of closure.
(STARTIGNORE)
(Hint):
(Subject): Closures
(Difficulty): Intermediate
(Applicability): Course 
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Closures bind values to variables in the returned function as early as possible in the program
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): Closures bind values at the last possible moment, which means you need to pay close attention to return times and the state of variables when you are using closures.
(WF): Closures bind values at the last possible moment, which means you need to pay close attention to return times and the state of variables when you are using closures.
(STARTIGNORE)
(Hint):
(Subject): Closures
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the importance of knowing load order and hoisting concepts?
(A): Load order is not important when working with JavaScript, but hoisting refers to the important concept of passing parameters
(B): These concepts affect what variables and objects are available to different blocks of code during runtime
(C): These concepts determine the naming conventions of variables and functions
(D): All functions expressions are hoisted to the top of their scope and are available to that scope regardless of load order
(Correct): B 
(Points): 1
(CF): Both load order and hoisting are important because it determines what values are stored in variables and available to other parts of your program. These concepts don’t relate to naming conventions, and function expressions are never hoisted (they are treated as assignments).
(WF): Both load order and hoisting are important because it determines what values are stored in variables and available to other parts of your program. These concepts don’t relate to naming conventions, and function expressions are never hoisted (they are treated as assignments).
(STARTIGNORE)
(Hint):
(Subject): Hoisting
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the general definition of a JavaScript object?
(A): A container of related information stored in properties of the object
(B): Arrays and Numbers are the only objects in JavaScript
(C): A reusable block of code that takes an input and gives an output
(D): An anonymous function assigned to a variable
(E): A capitalized function that is instantiated with the ‘new’ keyword
(Correct): A 
(Points): 1
(CF): An object is just a container of related information. There are many different types of objects in JavaScript.
(WF): An object is just a container of related information. There are many different types of objects in JavaScript.
(STARTIGNORE)
(Hint):
(Subject): Objects
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): What is true about the behavior of the “delete” operator when it is used on an object? (choose 2)
(A): The delete operator is only used to delete entire objects
(B): The delete operator returns true if it deleted a property, and false if the property did not exist 
(C): The delete operator always returns true when used on an object, regardless of whether a property was deleted
(D): The delete operator deletes the entire property from an object (property name and value)
(E): The delete operator deletes only the value of a property of an object but leaves the property as part of the object with a value of undefined
(Correct): C, D
(Points): 1
(CF): The delete operator can be used to delete properties from objects and it always deletes the entire property and value. This operator always returns true when used on an object even if the property didn’t exist when the delete statement was executed.
(WF): The delete operator can be used to delete properties from objects and it always deletes the entire property and value. This operator always returns true when used on an object even if the property didn’t exist when the delete statement was executed.
(STARTIGNORE)
(Hint):
(Subject): Objects 
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): The ______.prototype is the highest level prototype that every JavaScript object gets
(A): Array
(B): Function
(C): Object
(D): Variable
(Correct): C
(Points): 1
(CF): JavaScript objects can inherit many object prototypes, but they all at least inherit the Object.prototype as a basic object blueprint.
(WF): JavaScript objects can inherit many object prototypes, but they all at least inherit the Object.prototype as a basic object blueprint.
(STARTIGNORE)
(Hint): 
(Subject): Prototypes
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): The [code]Object.create()[/code] method can be used to create a new object that is a copy of the original object. If you change the properties of the new object, do the corresponding properties on the original duplicated object change as well?
(A): Yes
(B): No
(C): Only if the <originalObject>.prototype is the duplicated object’s only prototype object
(D): Only if you use the [code]force[/code] operator when making the changes to the properties
(Correct): B
(Points): 1
(CF): The original object’s properties are not affected by changing the values of the new object’s properties.
(WF): The original object’s properties are not affected by changing the values of the new object’s properties.
(STARTIGNORE)
(Hint):
(Subject): Prototypes
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the syntax to check if the prototype of <obj1> exists in the prototype chain of <obj2>?
(A): [code]<obj1>.prototypeOf(<obj2>);[/code]
(B): [code]<obj1>.isPrototypeOf(<obj2>);[/code]
(C): [code]<obj2>.isPrototypeOf(<obj1>);[/code]
(D): [code]<obj1>.__proto__(<obj2>);[/code]
(E): [code]<obj2>.hasParentPrototype(<obj1>);[/code]
(Correct): B
(Points): 1
(CF): The [code]isPrototypeOf()[/code] method allows you to check whether or not an object(<obj1>) exists within another object's(<obj2>) prototype chain. 
(WF): The [code]isPrototypeOf()[/code] method allows you to check whether or not an object(<obj1>) exists within another object's(<obj2>) prototype chain. 
(STARTIGNORE)
(Hint):
(Subject): Prototypes
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
