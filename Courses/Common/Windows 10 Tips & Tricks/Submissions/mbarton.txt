(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton
(Course Site): Lynda
(Course Name): Windows 10: Tips & Tricks
(Course URL): https://www.lynda.com/Windows-tutorials/Windows-10-Tips-Tricks/417146-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): What feature does Windows 10 provide for temporarily turning off notifications in the Action Center?
(A): Quiet Hours
(B): Do Not Disturb
(C): Silent Mode
(D): Sleepy Time
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 0
(Question): True or False: Windows 10 has both Internet Explorer and Microsoft Edge available to use.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): What is the purpose of the Task View button in the Windows 10 Taskbar?
(A): Shows all of your running apps
(B): Shows any virtual desktops
(C): Shows running apps and virtual desktops
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): Windows 10 now lets you download Windows Updates from locations other than Microsoft's servers.  Which of the following is NOT a supported alternate location.
(A): Other PCs on your local network
(B): Other PCs on the internet
(C): Other PCs your Microsoft account is associated with, regardless of the network they are on
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): What does the keyboard shortcut Windows+L do?
(A): Locks your screen
(B): Loads the disk in your CD/DVD drive
(C): The highlighted word is learned by the spellchecker
(D): The photo being edited is rotated left
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 0
(Question): True or False: In Windows 10 you can use CTRL+C, CTRL+V, and CTRL+X in a command prompt.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): You can easily switch in and out of Tablet Mode from which location?
(A): Right Click on Desktop and select Tablet Mode On or Tablet Mode Off
(B): Toggle the icon in the Action Center
(C): The System option in Windows Settings
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): What does the keyboard shortcut Windows+I do?
(A): Opens Windows Explorer
(B): Locks the screen
(C): Inserts text from the clipboard 
(D): Opens Windows Settings
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): Which of the following are available calculator modes in the Windows 10 Calculator app?
(A): Scientific
(B): Programmer
(C): Temperature Converter
(D): Language Converter
(E): Volume Converter
(Correct): A,B,C,E
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 28
(Grade style): 0
(Random answers): 1
(Question): What is the Reading List feature in the Microsoft Edge browser?
(A): A way for you to save articles to read later
(B): A way for you to see what books your friends are reading
(C): A way for Microsoft to recommend articles for you to read
(D): A way for Microsoft to recommend books for you to read
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Windows 10
(Difficulty):  Beginner
(Applicability): General 
(ENDIGNORE)
