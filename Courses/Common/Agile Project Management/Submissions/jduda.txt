(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): Lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a role of the project manager?
(A): Observing and guiding the team.
(B): To lead via coaching.
(C): Setting the priority of features for the team.
(D): Clearing roadblocks the team encounters.
(E): Listening during the daily standups.
(Correct): C
(Points): 1
(CF): The overall priority of features should be set by the customer. Within the scrum, the team decides on priority. A good project manager will fill all the other mentioned roles.
(WF): The overall priority of features should be set by the customer. Within the scrum, the team decides on priority. A good project manager will fill all the other mentioned roles.
(STARTIGNORE)
(Hint): 
(Subject): Agile Project
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following does results in the the closing stage of an agile project? (Select 3)
(A): All features of the project have been implemented.
(B): The product owner wants to change the priority in the backlog.
(C): The project has run out of the allocated time.
(D): The project has exceeded its allocated budget.
(E): There is a new project manager.
(Correct): A,C,D
(Points): 1
(CF): An agile project should only close when all features have been implemented, the has gone over the allocated time, or the budget is exceeded.
(WF): An agile project should only close when all features have been implemented, the has gone over the allocated time, or the budget is exceeded.
(STARTIGNORE)
(Hint): 
(Subject): Agile Project stages
(Difficulty): Moderate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Any software projected can be adapted to become an agile project.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Not all projects should be agile. For example, a project with clearly defined, unchanging requirements may not be suited for agile.
(WF): Not all projects should be agile. For example, a project with clearly defined, unchanging requirements may not be suited for agile.
(STARTIGNORE)
(Hint):
(Subject): Agile Project
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Snowplowing refers to which of the following?
(A): When you need to get your driveway cleared of snow before you can get to your daily standup.
(B): When the product owner keeps trying to add features mid-sprint.
(C): When a specific team member is the center of all conflict within the team.
(D): When unimplemented features are regularly added to the backlog at the end of the sprint.
(E): When a bug in one feature affects features already implemented in previous sprints.
(Correct): D
(Points): 1
(CF): If features are consistently being piled up into the backlog, the sprint needs to be reevaluated. Maybe the sprints are too short, estimates are inaccurate, or there is a problem in the team.
(WF): If features are consistently being piled up into the backlog, the sprint needs to be reevaluated. Maybe the sprints are too short, estimates are inaccurate, or there is a problem in the team.
(STARTIGNORE)
(Hint): 
(Subject): Agile Project
(Difficulty): Moderate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT part of the daily standup?
(A): Standing up.
(B): Resolving conflicts among the team.
(C): Switching the sequence in which team members present.
(D): Sharing wins the team has achieved.
(E): Inviting other stakeholders on a weekly basis.
(Correct): B
(Points): 1
(CF): Conflicts should be resolved outside of the daily standup only amongst the individuals involved.
(WF): Conflicts should be resolved outside of the daily standup only amongst the individuals involved.
(STARTIGNORE)
(Hint): 
(Subject): Agile Project
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)