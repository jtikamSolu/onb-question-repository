(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):Nikita Padmannavar
(Course Site):scrumguides.org
(Course Name):The Scrum Guide
(Course URL):http://www.scrumguides.org/docs/scrumguide/v1/Scrum-Guide-US.pdf#zoom=100
(Discipline):Professional
(ENDIGNORE)

(Type): multipleselect
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What pillars uphold every implementation of empirical process control?(choose 3)
(A): transparency
(B): translucency
(C): adaptation
(D): inspection
(E): adoption
(Correct): A, C, D
(Points): 1
(CF): Three pillars uphold every implementation of empirical process control: 
transparency, inspection, and adaptation. 
(WF): Three pillars uphold every implementation of empirical process control: 
transparency, inspection, and adaptation. 
(STARTIGNORE)
(Hint): Three pillars
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Inspection should be held frequently.
(A): true
(B): false
(Correct): B
(Points): 1
(CF): Inspection should not be too frequent as that may hinder work.
(WF): Inspection should not be too frequent as that may hinder work.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multipleselect
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Sprint Planning, Daily Scrum, Sprint Review, Sprint Retrospective are the events of (choose 2)
(A): adaptation
(B): inspection
(C): projection
(D): selection
(E): transparency
(Correct): A, E
(Points): 1
(CF): four formal events for inspection and adaptation:Sprint Planning, Daily Scrum, Sprint Review, Sprint Retrospective
(WF): four formal events for inspection and adaptation:Sprint Planning, Daily Scrum, Sprint Review, Sprint Retrospective
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Self-organizing teams means 
(A): how best to accomplish their work, by being directed by others outside the team.
(B): how best to accomplish their work, rather than being directed by others outside the team.
(C): have all competencies needed to accomplish the work by depending on others not part of the team.
(D): have all competencies needed to accomplish the work without depending on others not part of the team. 
(Correct): B
(Points): 1
(CF): how best to accomplish their work, rather than being directed by others outside the team.
(WF): how best to accomplish their work, rather than being directed by others outside the team.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who is responsible for managing the product backlog?
(A): Scrum master
(B): Product owner
(C): Development team
(D): Team leader
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Changes can be made anytime during the Sprint.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): During the Sprint, No changes are made that would endanger the Sprint Goal
(WF): During the Sprint, No changes are made that would endanger the Sprint Goal
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): A Sprint can be canceled before the Sprint time-box is over.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): A Sprint would be canceled if it no longer makes sense given the circumstances.
(WF): A Sprint would be canceled if it no longer makes sense given the circumstances.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The Sprint review is a
(A): status meeting
(B): formal meeting
(C): informal meeting
(D): decision making meeting
(Correct): C
(Points): 1
(CF): This is an informal meeting, not a status meeting, and the presentation of the Increment is intended to elicit feedback and foster collaboration.
(WF): This is an informal meeting, not a status meeting, and the presentation of the Increment is intended to elicit feedback and foster collaboration.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multipleselect
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): A Product backlog (choose 3)
(A): keeps changing 
(B): is never refined
(C): is never complete
(D): is dynamic
(Correct): A, C, D
(Points): 1
(CF): A Product Backlog is never complete. 
The Product Backlog is dynamic; it constantly changes to identify what the product needs to be appropriate, competitive, and useful.
(WF): A Product Backlog is never complete. 
The Product Backlog is dynamic; it constantly changes to identify what the product needs to be appropriate, competitive, and useful.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multipleselect
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): If artifacts are incompletely transparent (choose 3)
(A): it has no effect
(B): risk may increase
(C): decisions can be flawed
(D): everybody has the same understanding of the artifact
(E): value may diminish
(Correct): B, C, E
(Points): 1
(CF): If artifacts are incompletely transparent, these decisions can be flawed, value may diminish and risk may increase. 
(WF): If artifacts are incompletely transparent, these decisions can be flawed, value may diminish and risk may increase. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)