(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):Nikita Padmannavar
(Course Site):udemy.com
(Course Name):Communication Skills
(Course URL):https://www.udemy.com/consulting-skills-series-communication/
(Discipline):Professional
(ENDIGNORE)

(Type): multipleselect
(Category): 6
(Grade style): 1
(Random answers): 1
(Question): After a meeting, (choose 3)
(A): ask "in specifics" about progress on tasks
(B): follow up
(C): state your objectives
(D): use the developed hand-outs in follow up
(E): set GROUND rules
(Correct): A, B, D
(Points): 1
(WF): GROUND rules and objectives are stated during the meeting.
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):6
(Grade style): 0
(Random answers): 1
(Question): It is good to answer every out-of-topic question during the meeting because no questions are stupid.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Always stay on the topic.
(WF): Always stay on the topic.
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):6
(Grade style): 0
(Random answers): 1
(Question): Expected result should be decided before starting a conference call.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Have a purpose and have an expected result.
(WF): Have a purpose and have an expected result.
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multipleselect
(Category):6
(Grade style): 1
(Random answers): 1
(Question): What are good practices during any conference call? (choose 3)
(A): Do not set ground rules
(B): Share the call's purpose and intended results
(C): Do not waste time reviewing the agenda
(D): Use web demo tools for slides
(E): Call the meeting to order
(Correct): A, D, E
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):6
(Grade style): 0
(Random answers): 1
(Question): Call duration can change according to the needs and the call pace.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Set limits on call duration.
(WF): Set limits on call duration.
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multipleselect
(Category):6
(Grade style): 1
(Random answers): 1
(Question): When writing email which options are true? (choose 2)
(A): Humor can be used as much as we want.
(B): Know when email is not a good idea
(C): Reply promptly to serious messages
(D): Always attach CC and BCC
(Correct): B, C
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):6
(Grade style): 0
(Random answers): 1
(Question): In reality, 28% of time spent by consultants on emails is
(A): 40 hours/week
(B): 11.2 hours/week
(C): (40+11.2) hours/week
(D): 15 hours/week
(Correct): C
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):6
(Grade style): 0
(Random answers): 1
(Question): Multiple topics should be included in a single email.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): If your email has multiple topics, consider multiple emails
(WF): If your email has multiple topics, consider multiple emails
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multipleselect
(Category):6
(Grade style): 1
(Random answers): 1
(Question): In what ways can one hold the attention of the audience?(choose 2)
(A): Give little jolts
(B): Keep your presentation monotonous
(C): Do not tell stories
(D): Be creative professionally
(E): Talk to them- do not let them ask questions
(Correct): A, D
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):6
(Grade style): 0
(Random answers): 1
(Question): Which of the following can be offensive?
(A): Being comfortably assertive
(B): Measuring your style overtime
(C): Mirroring body language
(D): Considering the context always
(Correct): C
(Points): 1
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)